#include <iostream>
#include <cmath>
#include "libsimp.hpp"

double simpson(int n,double a,double b, double (*func)(double)){
    double dX = (b-a)/n;
    double output = 0;
    double xi;
    double xii;

    for (int i = 0; i < n; i++){
        xi = dX * i;
        xii = dX * (i+1);
        output += dX/6 * (func(xi) + 4*func((xi + xii)/2) + func(xii));
    }
 
    return output;
}