# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.27

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake.exe

# The command to remove a file.
RM = /usr/bin/cmake.exe -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/origi/.ssh/pt1_excersises/ex02/Ex2.2

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build

# Include any dependencies generated for this target.
include CMakeFiles/square.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/square.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/square.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/square.dir/flags.make

CMakeFiles/square.dir/ex2.2_main.cpp.obj: CMakeFiles/square.dir/flags.make
CMakeFiles/square.dir/ex2.2_main.cpp.obj: /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/ex2.2_main.cpp
CMakeFiles/square.dir/ex2.2_main.cpp.obj: CMakeFiles/square.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --progress-dir=/home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/square.dir/ex2.2_main.cpp.obj"
	/mingw64/bin/CC.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/square.dir/ex2.2_main.cpp.obj -MF CMakeFiles/square.dir/ex2.2_main.cpp.obj.d -o CMakeFiles/square.dir/ex2.2_main.cpp.obj -c /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/ex2.2_main.cpp

CMakeFiles/square.dir/ex2.2_main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Preprocessing CXX source to CMakeFiles/square.dir/ex2.2_main.cpp.i"
	/mingw64/bin/CC.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/ex2.2_main.cpp > CMakeFiles/square.dir/ex2.2_main.cpp.i

CMakeFiles/square.dir/ex2.2_main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Compiling CXX source to assembly CMakeFiles/square.dir/ex2.2_main.cpp.s"
	/mingw64/bin/CC.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/ex2.2_main.cpp -o CMakeFiles/square.dir/ex2.2_main.cpp.s

CMakeFiles/square.dir/libsimp.cpp.obj: CMakeFiles/square.dir/flags.make
CMakeFiles/square.dir/libsimp.cpp.obj: /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/libsimp.cpp
CMakeFiles/square.dir/libsimp.cpp.obj: CMakeFiles/square.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --progress-dir=/home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/square.dir/libsimp.cpp.obj"
	/mingw64/bin/CC.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/square.dir/libsimp.cpp.obj -MF CMakeFiles/square.dir/libsimp.cpp.obj.d -o CMakeFiles/square.dir/libsimp.cpp.obj -c /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/libsimp.cpp

CMakeFiles/square.dir/libsimp.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Preprocessing CXX source to CMakeFiles/square.dir/libsimp.cpp.i"
	/mingw64/bin/CC.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/libsimp.cpp > CMakeFiles/square.dir/libsimp.cpp.i

CMakeFiles/square.dir/libsimp.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Compiling CXX source to assembly CMakeFiles/square.dir/libsimp.cpp.s"
	/mingw64/bin/CC.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/libsimp.cpp -o CMakeFiles/square.dir/libsimp.cpp.s

# Object files for target square
square_OBJECTS = \
"CMakeFiles/square.dir/ex2.2_main.cpp.obj" \
"CMakeFiles/square.dir/libsimp.cpp.obj"

# External object files for target square
square_EXTERNAL_OBJECTS =

square: CMakeFiles/square.dir/ex2.2_main.cpp.obj
square: CMakeFiles/square.dir/libsimp.cpp.obj
square: CMakeFiles/square.dir/build.make
square: CMakeFiles/square.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --bold --progress-dir=/home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable square"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/square.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/square.dir/build: square
.PHONY : CMakeFiles/square.dir/build

CMakeFiles/square.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/square.dir/cmake_clean.cmake
.PHONY : CMakeFiles/square.dir/clean

CMakeFiles/square.dir/depend:
	cd /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/origi/.ssh/pt1_excersises/ex02/Ex2.2 /home/origi/.ssh/pt1_excersises/ex02/Ex2.2 /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build /home/origi/.ssh/pt1_excersises/ex02/Ex2.2/build/CMakeFiles/square.dir/DependInfo.cmake "--color=$(COLOR)"
.PHONY : CMakeFiles/square.dir/depend

