#include <iostream>
#include <cmath>
#include "libsimp.hpp"



double a = 0;
double b = 3.14159;
int N = 100;

double f(double x){
    return sin(x);
}

int main(){
    std::cout<<"Approx: \n";
    std::cout<<simpson(N,a,b,&f);
}