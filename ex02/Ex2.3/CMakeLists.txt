cmake_minimum_required(VERSION 3.26.4)

project(OLAS)

add_executable(${PROJECT_NAME} main.cpp)
