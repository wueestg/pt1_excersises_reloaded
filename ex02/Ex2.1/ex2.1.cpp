#include <iostream>
#define print std::cout<<
#define input std::cin>>
#include <vector>
#include <algorithm>


int n;
int main(){
print "Input the amount of integer numbers you wish to input:";

input n;
int p;

print "Input " << n << " numbers:";

std::vector<int> v;

for (int i = 0; i < n; i++){
    input p;
    v.push_back(p);
}

std::reverse(v.begin(), v.end());

for (int i: v){
    std::cout << i << " ";
}


return 0;

}