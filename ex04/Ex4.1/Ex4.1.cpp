#include <iostream>

using namespace std;

enum digit{Plus, Minus};

class Z2{
private:
    
    


public:
    digit state;
    Z2(digit r = Minus){
        state = r;
    }
    

    Z2 operator*(Z2 const& input){
        
        Z2 res;
        if ((state + input.state) % 2 == 1){
            res.state = Minus;
        }
        else {
            res.state = Plus;
        }


        return res;
    }

    void print(){
        if (state == Plus){
            cout << "Plus";
        }
        else {
            cout << "Minus";
        }
    }

    
};

template<class T>
T mypow(T a, unsigned int n)
{
    T res = a;
  for (int i; i<n; i++){
    res = res * a;
  }
  return res;
}



int main(){

    Z2 O1(Plus);
    Z2 O2(Minus);
    Z2 O3(Plus);

    Z2 O4 = mypow(O2, 3);
    O4.print();

}