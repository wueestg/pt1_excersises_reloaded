#include <iostream>
#include <functional>

double simpson(int n, double a, double b, std::function<double(double)> func){
    double dX = (b-a)/n;
    double output = 0;
    double xi;
    double xii;

    for (int i = 0; i < n; i++){
        xi = dX * i;
        xii = dX * (i+1);
        output += dX/6 * (func(xi) + 4*func((xi + xii)/2) + func(xii));
    }
 
    return output;
}

double simpson_2d(int n, double x_a, double x_b, double y_a, 
                  double y_b, std::function<double(double, double)> func){

    auto F = [n, y_a, y_b, func](double x) -> double {
        auto g = [x, func](double y) -> double{
            return func(x, y);
        };
    return simpson(n, y_a, y_b, g);
    };

    return simpson(n, x_a, x_b, F);
}

double func(double x, double y){
    return 3; // Or whatever function of x and y you'd like
}

int main(){
    std::cout << simpson_2d(100, 0, 1, 0, 1, func);
}
