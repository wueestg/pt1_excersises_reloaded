#include <iostream>

int main()
{
    float x = 1;
    
    float check = 1;

    float epsilon = 0.1;

    while(true){
        x = x + epsilon;
        if (x == check){
            break;
        }
        epsilon *= 0.5;
        x=1;
    }
    std::cout<< epsilon;
    std::cout<< "\n";

    while(true){
        x = x + epsilon;
        if (x != check){
            break;
        }
        epsilon *= 1.1;
        x=1;
    }

    std::cout<< epsilon;
    std::cout<< "\n";

    while(true){
        x = x + epsilon;
        if (x == check){
            break;
        }
        epsilon *= 0.99;
        x=1;
    }

    std::cout<<"Final number:\n";
    std::cout<< epsilon;

}
