#include <iostream>
#include <cmath>

double a = 0;
double b = 3.14159;
int N = 100;

double f(double x){
    return sin(x);
}

double simpson(int n,double a,double b){
    double dX = (b-a)/n;
    double output = 0;
    double xi;
    double xii;

    for (int i = 0; i < n; i++){
        xi = dX * i;
        xii = dX * (i+1);
        output += dX/6 * (f(xi) + 4*f((xi + xii)/2) + f(xii));
    }

    return output;
}



int main(){
    std::cout<<"Approx: \n";
    std::cout<<simpson(N,a,b);
}